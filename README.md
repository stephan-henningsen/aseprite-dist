# Unoffical Aseprite Ubuntu Package Repository #

You shouldn't be here.

## HOWTO ##

1. Add repository key: ```
wget -qO - https://bitbucket.org/stephan-henningsen/aseprite-dist/raw/master/aseprite-dist.pub.gpg  |  sudo apt-key add -```

2. Add repository itself: ```
echo deb [arch=amd64] https://bitbucket.org/stephan-henningsen/aseprite-dist/raw/master xenial main"  |  sudo tee /etc/apt/sources.list.d/aseprite-asklandd.list```

3. Install Aseprite: ```
sudo apt update  &&  sudo apt install aseprite```